<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Ventas $model */

$this->title = $model->n_venta;
\yii\web\YiiAsset::register($this);
?>
<div class="ventas-view bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'n_venta' => $model->n_venta], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'n_venta' => $model->n_venta], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'n_venta',
            'f_entrega',
            'cd_ticket',
        ],
    ]) ?>

</div>
