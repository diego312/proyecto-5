<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Reparaciones $model */

$this->title = $model->cd_reparacion;
\yii\web\YiiAsset::register($this);
?>
<div class="reparaciones-view bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cd_reparacion' => $model->cd_reparacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cd_reparacion' => $model->cd_reparacion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cd_reparacion',
            'motivo',
            'mot_detalles',
            'diagnostico',
            'f_recepcion',
            'coste',
            'estado:boolean',
            'n_venta',
            'cd_empleado',
        ],
    ]) ?>

</div>
