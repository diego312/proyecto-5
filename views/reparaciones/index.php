<?php

use app\models\Reparaciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\ReparacionesSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Reparaciones';
?>
<div class="reparaciones-index bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reparaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'cd_reparacion',
            'motivo',
            'mot_detalles',
            'diagnostico',
            'f_recepcion',
            'coste',
            'estado:boolean',
            'n_venta',
            'cd_empleado',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Reparaciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cd_reparacion' => $model->cd_reparacion]);
                 }
            ],
        ],
    ]); ?>


</div>
