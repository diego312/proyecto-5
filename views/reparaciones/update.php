<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Reparaciones $model */

$this->title = 'Update Reparaciones: ' . $model->cd_reparacion;
?>
<div class="reparaciones-update bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
