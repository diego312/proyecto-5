<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ReparacionesSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="reparaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cd_reparacion') ?>

    <?= $form->field($model, 'motivo') ?>

    <?= $form->field($model, 'mot_detalles') ?>

    <?= $form->field($model, 'diagnostico') ?>

    <?= $form->field($model, 'f_recepcion') ?>

    <?php // echo $form->field($model, 'coste') ?>

    <?php // echo $form->field($model, 'estado')->checkbox() ?>

    <?php // echo $form->field($model, 'n_venta') ?>

    <?php // echo $form->field($model, 'cd_empleado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
