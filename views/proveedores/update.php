<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Proveedores $model */

$this->title = 'Update Proveedores: ' . $model->nif;
?>
<div class="proveedores-update bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
