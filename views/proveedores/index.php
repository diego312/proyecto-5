<?php

use app\models\Proveedores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\ProveedoresSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Proveedores';
?>
<div class="proveedores-index bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Proveedores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'cd_proveedor',
            'nif',
            'nombre',
            'direccion',
            'email:email',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Proveedores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'cd_proveedor' => $model->cd_proveedor]);
                }
            ],
        ],
    ]);
    ?>


</div>
