<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = 'Update Telefonos: ' . $model->telefono;
?>
<div class="telefonos-update bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
