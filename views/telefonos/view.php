<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = $model->telefono;
\yii\web\YiiAsset::register($this);
?>
<div class="telefonos-view bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'telefono' => $model->telefono], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'telefono' => $model->telefono], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'telefono',
            'cd_proveedor',
        ],
    ]) ?>

</div>
