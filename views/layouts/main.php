<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Yii::getAlias('@web') . '/imagenes/favicon.ico']); ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="' . Yii::getAlias('@web') . '/imagenes/favicon?.ico" alt="" class="navbar-brand-img imagen-producto"> ' . Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Principal', 'url' => ['/site/index']],
            ['label' => 'Productos', 'url' => ['/site/catalogo']],
            Yii::$app->user->isGuest ? ( ['label' => ''] ) : (
                ['label' => 'Reparaciones', 'url' => ['/site/reparaciones']]
            ),
            Yii::$app->user->isGuest ? ( ['label' => ''] ) : (
                ['label' => 'Carrito', 'url' => ['/site/carrito']]
            ),
            Yii::$app->user->isGuest ? ( ['label' => ''] ) : (
                Yii::$app->user->identity->isAdmin(Yii::$app->user->identity->cd_ticket) ? ( 
                    ['label' => 'Panel de Administracion', 'url' => ['/site/administracion']] ) : ( ['label' => '']
                )
            ),
        ],
    ]);

     if (Yii::$app->user->isGuest) {
        $leftItems[] = ['label' => 'Iniciar sesión', 'url' => ['/site/login']];
    } else {
        $leftItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
            . Html::submitButton(
                'Cerrar sesión: ' . Yii::$app->user->identity->nombre,
                ['class' => 'btn btn-link logout bg-danger text-light']
            )
            . Html::endForm()
            . '</li>';
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ml-auto'],
        'items' => $leftItems,
    ]);
    
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left strong"> Technology Anonymity CORP &copy; <?= date('Y') ?><br><?php echo Html::a("Terminos y Condiciones", ['site/politicas']) ?></p>
        <p class="float-right">Contacto: <a href="mailto:serviciocliente@tacorp.com">serviciocliente@tacorp.com</a><br>Teléfono: 888 77 66 55</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
