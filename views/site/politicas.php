<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Technonymous';
$title = 'Politicas del Sitio Web';
?>
<div class="site-politicas">
    <h1><?= $title ?></h1>

    <h2>1. Política de Privacidad</h2>

    <p><strong>Recopilación y Uso de Datos Personales</strong></p>

    <p>
        Valoramos tu privacidad y la seguridad de tus datos personales. Antes de crear un usuario, solicitaremos tu consentimiento explícito para el tratamiento de datos. Puedes proporcionar información personal adicional, como DNI, nombre, apellidos, dirección, correo electrónico y teléfono, de manera voluntaria. Estos datos se manejan con total confidencialidad y no se comparten con terceros sin tu consentimiento.
    </p>

    <h2>2. Términos y Condiciones</h2>

    <p><strong>Uso del Sitio</strong></p>

    <p>
        Al acceder y utilizar nuestro sitio web, estás aceptando cumplir con los términos y condiciones establecidos en esta sección. Estos términos cubren todas las interacciones en nuestra plataforma, incluyendo procedimientos de compra, plazos de entrega, procesos de reparación y políticas de garantía.
    </p>

    <h2>3. Proceso de Compra y Entrega</h2>

    <p><strong>Opción de Pago</strong></p>

    <p>
        Ofrecemos la posibilidad de realizar pagos en nuestra tienda física.
    </p>

    <p><strong>Proceso de Compra</strong></p>

    <p>
        Nuestra plataforma te permite solicitar productos de manera sencilla y segura.
    </p>

    <p><strong>Plazo de Entrega</strong></p>

    <p>
        Los plazos de entrega varían según el proveedor y la distancia de transporte, con un rango que va desde 24 horas laborales hasta un mes. Te notificaremos la llegada de los productos a través de nuestro sitio web para mantenerte informado de manera oportuna.
    </p>

    <p><strong>Políticas de Entrega</strong></p>

    <p>
        Los productos adquiridos pueden ser recogidos en nuestra tienda física utilizando tu usuario y contraseña en el sitio web.
    </p>

    <h2>4. Reparaciones de Equipos Electrónicos</h2>

    <p><strong>Proceso de Solicitud de Reparación</strong></p>

    <p>
        Ofrecemos la posibilidad de solicitar reparaciones de equipos electrónicos mediante un formulario disponible tanto en nuestra tienda física como en línea.
    </p>

    <p><strong>Plazos de Reparación</strong></p>

    <p>
        El plazo de reparación es determinado por nuestro equipo técnico, y te proporcionamos información transparente sobre el plazo y el precio estimado antes de proceder con la reparación, para que puedas tomar decisiones informadas.
    </p>

    <p><strong>Políticas de Garantía</strong></p>

    <p>
        La garantía para sustitución de componentes coincide con la garantía ofrecida por el fabricante o proveedor de los componentes individuales. En el caso de reparaciones de hardware o software, garantizamos la calidad de nuestro trabajo durante un período de 6 meses.
    </p>

    <h2>5. Creación de Usuarios</h2>

    <p><strong>Información del Perfil</strong></p>

    <p>
        Antes de crear un usuario, solicitamos tu consentimiento para el tratamiento de datos personales. Puedes enriquecer tu perfil proporcionando información personal adicional. Toda la información es almacenada y protegida de forma segura. Las contraseñas se almacenan utilizando técnicas avanzadas de cifrado para garantizar la seguridad de tu cuenta.
    </p>

    <h2>6. Medidas de Seguridad</h2>

    <p><strong>Acceso a la Información Personal</strong></p>

    <p>
        La información personal está resguardada y solo es accesible para los administradores de nuestro sitio web, quienes operan con estándares rigurosos de seguridad para proteger tu privacidad.
    </p>

    <h2>7. Política de Devolución y Reembolso</h2>

    <p><strong>Garantía de Productos</strong></p>

    <p>
        Las políticas de devolución y reembolso están en línea con las políticas de garantía de los fabricantes y proveedores de los productos que ofrecemos, garantizando una experiencia de compra justa y confiable.
    </p>

    <h2>8. Contacto</h2>

    <p><strong>Información de Contacto</strong></p>

    <p>
        Para cualquier consulta, pregunta o asistencia, ponemos a disposición de nuestros clientes la siguiente información de contacto:<br>
        Correo Electrónico: <a href="mailto:serviciocliente@tacorp.com">serviciocliente@tacorp.com</a><br>
        Teléfono: 888 77 66 55.
    </p>

    <h2>9. Derechos de los Usuarios</h2>

    <p><strong>Derechos de Privacidad</strong></p>

    <p>
        Respetamos tus derechos de privacidad. Tienes el derecho de acceder, rectificar, suprimir, limitar el tratamiento y portar tus datos personales. Puedes ejercer estos derechos enviando una solicitud a nuestra dirección de correo electrónico de contacto.
    </p>

    <h2>10. Cambios en las Políticas</h2>

    <p><strong>Notificación de Cambios</strong></p>

    <p>
        En caso de cambios en nuestras políticas, notificaremos a nuestros usuarios la primera vez que ingresen a nuestro sitio web después de la modificación, garantizando la transparencia y la oportunidad en la comunicación.
    </p>

    <h2>11. Legislación Aplicable</h2>

    <p><strong>Ley Aplicable</strong></p>

    <p>
        Estas políticas se rigen por la legislación de la Unión Europea, siendo este el marco legal que rige todas las operaciones y relaciones dentro de nuestra plataforma. Cualquier disputa se resolverá de acuerdo con las leyes aplicables.
    </p>
</div>

