<?php

/** @var yii\web\View $this */
use yii\helpers\Html;


?>

<div class="col-md-12 mb-4">
    <div class="card bg-dark text-white">
        <div class="row no-gutters">
            <div class="col-md-7">
                <div class="card-body">
                    <h5 class="card-title"><?= $model->motivo ?></h5>
                    <p class="card-text"><strong>Detalles:</strong> <?= $model->mot_detalles ?></p>
                    <p class="card-text"><strong>Diagnóstico:</strong> <?= $model->diagnostico ?></p>
                    <p class="card-text"><strong>Estado:</strong> <?= $model->estado !== null ? ($model->estado ? 'Completado' : 'En proceso') : 'Pendiente' ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body">
                    <?php if ($model->cdEmpleado === null): ?>
                        <p class="card-text"><strong>Tecnico:</strong> <?= '' ?> <?= '' ?></p>
                    <?php else: ?>
                        <p class="card-text"><strong>Tecnico:</strong> <?= $model->cdEmpleado->nombre ?> <?= $model->cdEmpleado->apellidos ?></p>
                    <?php endif; ?>
                    <p class="card-text"><strong>Fecha de recogida:</strong> <?= Yii::$app->formatter->asDate($model->f_recepcion, 'php:d/m/Y') ?></p>
                    <p class="card-text"><strong>Coste:</strong> <?= $model->coste ?>€</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card-body">
                    <?php if ($model->estado === null): ?>
                        <?= Html::a('Cancelar', ['site/cancelar-reparaciones', 'id' => $model->cd_reparacion], ['class' => 'btn btn-danger']) ?>
                        <p></p>
                        <?= Html::a('Aprobar', ['site/aprobar-reparaciones', 'id' => $model->cd_reparacion], ['class' => 'btn btn-primary']) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>