<?php

/** @var yii\web\View $this */
use yii\widgets\ListView;

$this->title = 'Technonymous';
$title = 'Productos';
?>
<div>
    <h1><?= $title ?></h1>   
    <div class="row">              
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_catalogo',
            'summary' => false,
            'layout' => "{items}",
            'options' => ['class' => 'grid'],
        ]); ?> 
    </div>
</div>
