<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Technonymous';
$title = 'Crear Cuenta';
?>
<h1><?= $title ?></h1>
<?php $form = ActiveForm::begin(); ?>
<div class="bg-dark p-3">
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'inputType' => 'password']) ?>
</div>
<h2>Esos campos son opcionales:</h2>
<div class="bg-dark p-3">
    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton('Confirmar', ['class' => 'btn btn-success']) ?>
    </div> 
</div>
<?php ActiveForm::end(); ?>