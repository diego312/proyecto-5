<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Technonymous';
?>

<div>
    <h1>Gestión de registros</h1>
    <div class="row">
        <?php foreach ($nombres as $index => $nombre) : ?>
            <div class="col-md-3">
                <div class="card alturaminima alturamaxima bg-light">
                    <div class="card-body">
                        <h5 class="card-title"><?= $this->context->actionEnlace($nombre, $enlaces[$index]) ?></h5>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <h1>Gestión de reparaciones</h1>
    <div class="row">              
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_reparacionesadministracion',
            'summary' => false,
            'layout' => "{items}",

        ]) ?>
    </div>
</div>