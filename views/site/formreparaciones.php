<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Reparaciones $model */

?>
<div class="reparaciones-create bg-dark p-3">
    <?php if ($model->motivo !== null) : ?>
        <h1>Presupuestar Reparacion</h1>
        <?= $this->render('_formreparacionesadmin', ['model' => $model]) ?>
    <?php else : ?>
        <h1>Solicitar Reparacion</h1>
        <?= $this->render('_formreparacionesclient', ['model' => $model]) ?>
    <?php endif; ?>
</div>
