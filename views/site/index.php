<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

$this->title = 'Technonymous';
$this->registerJs(
    "
    $(document).ready(function () {
        if (" . (Yii::$app->session->hasFlash('loginSuccess') ? 'true' : 'false') . ") {
            $('#success-modal').modal('show');
        } else if (" . (Yii::$app->session->hasFlash('loginError') ? 'true' : 'false') . ") {
            $('#error-modal').modal('show');
        } else if (" . (Yii::$app->session->hasFlash('logout') ? 'true' : 'false') . ") {
            $('#logout-modal').modal('show');
        }
    });
    ",
    View::POS_READY
);

if (Yii::$app->session->hasFlash('loginSuccess')) {
    Modal::begin([
        'id' => 'success-modal',
        'title' => 'Sesión',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-success',]
    ]);
    echo '<p class="text-success">' . Yii::$app->session->getFlash('loginSuccess') . '</p>';
    Modal::end();
} else if (Yii::$app->session->hasFlash('loginError')) {
    Modal::begin([
        'id' => 'error-modal',
        'title' => 'Ups',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-dark',]
    ]);
    echo '<p class="text-danger">' . Yii::$app->session->getFlash('loginError') . '</p>';
    Modal::end();
} else if (Yii::$app->session->hasFlash('logout')) {
    Modal::begin([
        'id' => 'logout-modal',
        'title' => 'Sesión',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-purple',]
    ]);
    echo '<p class="txt-purple">' . Yii::$app->session->getFlash('logout') . '</p>';
    Modal::end();
}
?>

<div>
    <div id="ejemploCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#ejemploCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#ejemploCarousel" data-slide-to="1"></li>
            <li data-target="#ejemploCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <?= Html::img('@web/imagenes/Carro1.jpg', ['alt' => 'Foto de la tienda', 'class' => 'd-block w-100 rounded']) ?>
            </div>
            <div class="carousel-item">
                <?= Html::img('@web/imagenes/Carro2.jpg', ['alt' => 'Productos en oferta', 'class' => 'd-block w-100 rounded']) ?>
            </div>
            <div class="carousel-item">
                <?= Html::img('@web/imagenes/Carro3.jpg', ['alt' => 'Catalogo', 'class' => 'd-block w-100 rounded']) ?>
            </div>
        </div>
        <a class="carousel-control-prev" href="#ejemploCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#ejemploCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Siguiente</span>
        </a>
    </div>

    <div class="site-index">

        <div class="jumbotron text-center bg-transparent">
            <h1 class="display-4">Technonymous</h1>

            <p class="lead">Una pagina para comprar desde el anonimato</p>
        </div>
    </div> 
</div>
