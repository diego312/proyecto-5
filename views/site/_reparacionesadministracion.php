<?php

/** @var yii\web\View $this */
use yii\helpers\Html;


?>

<div class="col-md-12 mb-3">
    <div class="card bg-dark text-white">
        <div class="row no-gutters">
            <div class="col-md-3">
                <div class="card-body">
                    <h5 class="card-title"><?= $model->motivo ?></h5>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-body">
                    <p class="card-text"><strong>Detalles:</strong> <?= $model->mot_detalles ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body">
                    <p class="card-text"><strong>Estado:</strong> <?= $model->estado !== null ? ($model->estado ? 'Completado' : 'En proceso') : 'Pendiente' ?></p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="card-body">
                    <?php if ($model->estado === null): ?>
                        <?= Html::a('Presupuestar', ['site/form-reparaciones', 'id' => $model->cd_reparacion], ['class' => 'btn btn-primary']) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>