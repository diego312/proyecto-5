<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Reparaciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="reparaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'diagnostico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'f_recepcion')->textInput() ?>

    <?= $form->field($model, 'coste')->textInput() ?>

    <?= $form->field($model, 'cd_empleado')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
