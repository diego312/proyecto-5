<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

?>

<div class="col-md-12 mb-4">
    <div class="card">
        <?= Html::a(Html::img("@web/imagenes/producto" . $model->cd_producto . ".jpg", ['class' => 'card-img-top']), ['site/detalle-catalogo', 'id' => $model->cd_producto]) ?>
        <div class="card-body">
            <h5 class="card-title text-dark"><?= Html::a($model->nombre, ['site/detalle-catalogo', 'id' => $model->cd_producto]) ?></h5>
            <p class="card-text text-dark">Precio: <?= $model->precio ?>€</p>
            <p class="card-text text-dark"><?= $model->descripcion ?></p>
        </div>
        <div class="card-footer">
            <?= Html::a('Ver Detalles', ['site/detalle-catalogo', 'id' => $model->cd_producto], ['class' => 'btn btn-purple']) ?>
        </div>
    </div>
</div>


