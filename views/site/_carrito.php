<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

?>

<div class="col-md-12 mb-4">
    <div class="card bg-dark text-white">
        <div class="row no-gutters">
            <div class="col-md-1 fix-size">
                <?= Html::a(Html::img("@web/imagenes/producto" . $model->cdProducto->cd_producto . ".jpg", ['class' => 'card-img']), ['site/detalle-catalogo', 'id' => $model->cdProducto->cd_producto]) ?>
            </div>
            <div class="col-md-4">
                <div class="card-body">
                    <h5 class="card-title"><?= $model->cdProducto->nombre ?></h5>
                    <p class="card-text">Precio: <?= $model->cdProducto->precio ?>€</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card-body">
                    <p class="card-text">Cantidad x<strong><?= $model->cantidad ?></strong></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-body">
                    <?= Html::a('Solicitar cancelacion', ['site/cancelar-carrito'], ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>
</div>