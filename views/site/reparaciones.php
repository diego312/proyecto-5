<?php

/** @var yii\web\View $this */
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

$this->title = 'Technonymous';
$title = 'Reparaciones';$this->registerJs(
    "
    $(document).ready(function () {
        if (" . (Yii::$app->session->hasFlash('aprobarReparacion') ? 'true' : 'false') . ") {
            $('#aprobar-modal').modal('show');
        } else if (" . (Yii::$app->session->hasFlash('cancelarReparacion') ? 'true' : 'false') . ") {
            $('#cancelar-modal').modal('show');
        } else if (" . (Yii::$app->session->hasFlash('crearReparacion') ? 'true' : 'false') . ") {
            $('#crear-modal').modal('show');
        }
    });
    ",
    View::POS_READY
);

if (Yii::$app->session->hasFlash('aprobarReparacion')) {
    Modal::begin([
        'id' => 'aprobar-modal',
        'title' => 'Reparación aprobada',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-success',]
    ]);
    echo '<p class="text-success">' . Yii::$app->session->getFlash('aprobarReparacion') . '</p>';
    Modal::end();
} else if (Yii::$app->session->hasFlash('cancelarReparacion')) {
    Modal::begin([
        'id' => 'cancelar-modal',
        'title' => 'Reparación cancelada',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-danger',]
    ]);
    echo '<p class="text-danger">' . Yii::$app->session->getFlash('cancelarReparacion') . '</p>';
    Modal::end();
} else if (Yii::$app->session->hasFlash('crearReparacion')) {
    Modal::begin([
        'id' => 'crear-modal',
        'title' => 'Solicitud de Reparación',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-success',]
    ]);
    echo '<p class="text-success">' . Yii::$app->session->getFlash('crearReparacion') . '</p>';
    Modal::end();
}
?>
<div>
    <h1>
        <?= $title ?>
        <?= Html::a('Solicitar Reparacion', ['site/form-reparaciones', 'id' => 'newForm'], ['class' => 'btn btn-primary']) ?>
    </h1>
    <?php if ($dataProvider->getCount() > 0): ?>
        <div class="row">              
            <?= 
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_reparaciones',
                'summary' => false,
                'layout' => "{items}",
            ]) ?>
        </div>
    <?php else: ?>
        <p>No hay reparaciones.</p>
    <?php endif; ?>
</div>
