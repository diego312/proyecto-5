<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = 'Technonymous';
$title = 'Productos';
$this->registerJs(
    "
    $(document).ready(function () {
        $('#politicas-modal').modal('show');
    });
    ",
    View::POS_READY
);
if (Yii::$app->session->hasFlash('politicas')) {
    Modal::begin([
        'id' => 'politicas-modal',
        'title' => 'Políticas',
        'size' => Modal::SIZE_EXTRA_LARGE,
        'headerOptions' => [
            'class' => 'bg-purple',]
    ]);
    echo Yii::$app->session->getFlash('politicas');
    $form = ActiveForm::begin([
        'action' => ['site/crear-carrito'],
        'method' => 'post',
    ]);
    echo Html::hiddenInput('id', $producto->cd_producto);
    echo Html::hiddenInput('cantidad', $cantidad);
    echo Html::submitButton('Añadir al carrito', ['class' => 'btn btn-purple']);
    ActiveForm::end();
    Modal::end();

}
?>

<div>
    <h1><?= $producto->nombre ?></h1>

    <div class="row">
        <div class="col-md-6">
            <div class="imagen-producto">
                <?= Html::img(Yii::getAlias('@web') . "/imagenes/producto" . $producto->cd_producto . ".jpg") ?>
            </div>
        </div>
        <div class="col-md-6 bg-dark rounded p-3">
            <p><strong>Precio: <?= $producto->precio ?>€</strong></p>
            <p>Info: <?= $producto->descripcion ?></p>
            <p><strong>Datos del proveedor: </strong></p>
            <p class="border border-white rounded p-3">
                <strong>Nombre:</strong> <?= $proveedor->nombre ?><br>
                <strong>Teléfono/s:</strong><br>
                <?php foreach ($telefonos as $telefono): ?>
                    <?= $telefono->telefono ?><br>
                <?php endforeach; ?>
                <strong>Dirección:</strong> <?= $proveedor->direccion ?>
            </p>
            <form id="solicitud-form" action="<?= Yii::$app->urlManager->createUrl(['site/aceptar-politicas']) ?>" method="post">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>">
                <input type="hidden" id="cd_producto" name="cd_producto" value="<?= $producto->cd_producto ?>">
                <div class="form-group d-flex align-items-center">
                    <label for="cantidad" class="mr-2">Cantidad:</label>
                    <div class="col-md-2 input-group flex-grow-1">
                        <input type="number" id="cantidad" name="cantidad" class="form-control input-number" value="1" min="1" max="99">
                    </div>
                </div>
                <?= Html::submitButton('Solicitar Producto', ['class' => 'btn btn-purple', 'id' => 'solicitarBtn']) ?>
            </form>
            <script>
                $(document).ready(function () {
                    $('.btn-number').click(function (e) {
                        e.preventDefault();

                        var fieldName = $(this).attr('data-field');
                        var type = $(this).attr('data-type');
                        var input = $("input[name='" + fieldName + "']");
                        var currentVal = parseInt(input.val());

                        if (!isNaN(currentVal)) {
                            if (type === 'minus') {
                                if (currentVal > input.attr('min')) {
                                    input.val(currentVal - 1).change();
                                }
                            } else if (type === 'plus') {
                                if (currentVal < input.attr('max')) {
                                    input.val(currentVal + 1).change();
                                }
                            }
                        }
                    });

                    $('.input-number').focusin(function () {
                        $(this).data('oldValue', $(this).val());
                    });

                    $('.input-number').change(function () {
                        var minValue = parseInt($(this).attr('min'));
                        var maxValue = parseInt($(this).attr('max'));
                        var valueCurrent = parseInt($(this).val());

                        if (valueCurrent >= minValue) {
                            $(".btn-number[data-type='minus'][data-field='" + $(this).attr('name') + "']").removeAttr('disabled');
                        } else {
                            alert('Sorry, the minimum value was reached');
                            $(this).val($(this).data('oldValue'));
                        }

                        if (valueCurrent <= maxValue) {
                            $(".btn-number[data-type='plus'][data-field='" + $(this).attr('name') + "']").removeAttr('disabled');
                        } else {
                            alert('Sorry, the maximum value was reached');
                            $(this).val($(this).data('oldValue'));
                        }
                    });
                });
            </script>
        </div>
    </div>
</div>