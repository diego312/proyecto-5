<?php

/** @var yii\web\View $this */
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\web\View;

$this->title = 'Technonymous';
$title = 'Carrito';
$this->registerJs(
    "
    $(document).ready(function () {
        if (" . (Yii::$app->session->hasFlash('crearCarrito') ? 'true' : 'false') . ") {
            $('#crear-modal').modal('show');
        } else if (" . (Yii::$app->session->hasFlash('guardarCarrito') ? 'true' : 'false') . ") {
            $('#guardar-modal').modal('show');
        } else if (" . (Yii::$app->session->hasFlash('eliminarCarrito') ? 'true' : 'false') . ") {
            $('#eliminar-modal').modal('show');
        } else if (" . (Yii::$app->session->hasFlash('cancelarCarrito') ? 'true' : 'false') . ") {
            $('#cancelar-modal').modal('show');
        }
    });
    ",
    View::POS_READY
);

if (Yii::$app->session->hasFlash('crearCarrito')) {
    Modal::begin([
        'id' => 'crear-modal',
        'title' => 'Añadir al Carrito',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-success',]
    ]);
    echo '<p class="text-success">' . Yii::$app->session->getFlash('crearCarrito') . '</p>';
    Modal::end();
} else if (Yii::$app->session->hasFlash('guardarCarrito')) {
    Modal::begin([
        'id' => 'guardar-modal',
        'title' => 'Productos pedidos',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-success',]
    ]);
    echo '<p class="text-success">' . Yii::$app->session->getFlash('guardarCarrito') . '</p>';
    Modal::end();
} else if (Yii::$app->session->hasFlash('eliminarCarrito')) {
    Modal::begin([
        'id' => 'eliminar-modal',
        'title' => 'Carrito',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-danger',]
    ]);
    echo '<p class="text-danger">' . Yii::$app->session->getFlash('eliminarCarrito') . '</p>';
    Modal::end();
} else if (Yii::$app->session->hasFlash('cancelarCarrito')) {
    Modal::begin([
        'id' => 'cancelar-modal',
        'title' => 'Solicitar cancelacion',
        'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>',
        'headerOptions' => [
            'class' => 'bg-purple',]
    ]);
    echo '<p class="txt-purple">' . Yii::$app->session->getFlash('cancelarCarrito') . '</p>';
    Modal::end();
}
?>
<div>
    <h1><?= $title ?></h1> 
    <?php if (count($carrito) > 0): ?>
        <div class="row">
            <?php foreach ($carrito as $item): ?>
                <div class="col-md-12 mb-4">
                    <div class="card bg-dark text-white">
                        <div class="row no-gutters">
                            <div class="col-md-1 fix-size">
                                <?= Html::a(Html::img("@web/imagenes/producto" . $item['id'] . ".jpg", ['class' => 'card-img']), ['site/detalle-catalogo', 'id' => $item['id']]) ?>
                            </div>
                            <div class="col-md-4">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $item['nombre'] ?></h5>
                                    <p class="card-text">Precio: <?= $item['precio'] ?>€</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card-body">
                                    <p class="card-text">Cantidad x<strong><?= $item['cantidad'] ?></strong></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card-body">
                                    <?= Html::a('Eliminar', ['site/eliminar-carrito', 'id' => $item['id']], ['class' => 'btn btn-danger']) ?>
                                    <?= Html::a('Modificar', ['site/modificar-carrito', 'id' => $item['id']], ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="col-md-12 mb-4">
                <?= Html::a('Finalizar Pedido', ['site/guardar-carrito'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    <?php else: ?>
        <p>No hay productos en la cesta.</p>
    <?php endif; ?>
    <h2>Pendientes de envio</h2>
    <?php if ($dataProvider->getCount() > 0): ?>
        <div class="row">              
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_carrito',
                'summary' => false,
                'layout' => "{items}",
            ]);
            ?> 
        </div>
    <?php else: ?>
        <p>No hay productos pedidos.</p>
    <?php endif; ?>
</div>
