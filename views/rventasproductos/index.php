<?php

use app\models\Rventasproductos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\RventasproductosSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Relación Ventas-Productos';
?>
<div class="rventasproductos-index bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Relación Ventas-Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'n_venta',
            'cd_producto',
            'cantidad',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Rventasproductos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]);
    ?>


</div>
