<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Rventasproductos $model */

$this->title = 'Update Relación Ventas-Productos: ' . $model->id;
?>
<div class="rventasproductos-update bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
