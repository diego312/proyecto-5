<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Rventasproductos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="rventasproductos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'n_venta')->textInput() ?>

    <?= $form->field($model, 'cd_producto')->textInput() ?>
    
    <?= $form->field($model, 'cantidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
