<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Tickets $model */

$this->title = $model->cd_ticket;
\yii\web\YiiAsset::register($this);
?>
<div class="tickets-view bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cd_ticket' => $model->cd_ticket], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cd_ticket' => $model->cd_ticket], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cd_ticket',
            'dni',
            'nombre',
            'apellidos',
            'direccion',
            'email:email',
            'telefono',
            //'password',
        ],
    ]) ?>

</div>
