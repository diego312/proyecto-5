<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Empleados $model */

$this->title = $model->dni;
\yii\web\YiiAsset::register($this);
?>
<div class="empleados-view bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'dni' => $model->dni], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'dni' => $model->dni], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni',
            'nombre',
            'apellidos',
            'direccion',
            'email:email',
            'telefono',
        ],
    ]) ?>

</div>
