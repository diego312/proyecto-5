<?php

use app\models\Empleados;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\EmpleadosSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title = 'Empleados';
?>
<div class="empleados-index bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Empleados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'Nº'],
            'dni',
            'nombre',
            'apellidos',
            'direccion',
            'email:email',
            'telefono',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Empleados $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dni' => $model->dni]);
                }
            ],
        ],
    ]);
    ?>


</div>
