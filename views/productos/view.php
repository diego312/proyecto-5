<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */

$this->title = $model->cd_producto;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-view bg-dark p-3">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cd_producto' => $model->cd_producto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cd_producto' => $model->cd_producto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cd_producto',
            'nombre',
            'precio',
            'descripcion',
            'cd_proveedor',
        ],
    ]) ?>

</div>
