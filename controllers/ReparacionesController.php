<?php

namespace app\controllers;

use app\models\Reparaciones;
use app\models\ReparacionesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;

/**
 * ReparacionesController implements the CRUD actions for Reparaciones model.
 */
class ReparacionesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAdmin(Yii::$app->user->identity->cd_ticket);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reparaciones models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ReparacionesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reparaciones model.
     * @param int $cd_reparacion Cd Reparacion
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cd_reparacion)
    {
        return $this->render('view', [
            'model' => $this->findModel($cd_reparacion),
        ]);
    }

    /**
     * Creates a new Reparaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Reparaciones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cd_reparacion' => $model->cd_reparacion]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Reparaciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cd_reparacion Cd Reparacion
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cd_reparacion)
    {
        $model = $this->findModel($cd_reparacion);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cd_reparacion' => $model->cd_reparacion]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Reparaciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cd_reparacion Cd Reparacion
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cd_reparacion)
    {
        $this->findModel($cd_reparacion)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reparaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cd_reparacion Cd Reparacion
     * @return Reparaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cd_reparacion)
    {
        if (($model = Reparaciones::findOne(['cd_reparacion' => $cd_reparacion])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
