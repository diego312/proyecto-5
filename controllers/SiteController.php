<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\data\ActiveDataProvider;
use app\models\Productos;
use app\models\Proveedores;
use app\models\Telefonos;
use app\models\Reparaciones;
use app\models\Ventas;
use app\models\Rventasproductos;
use app\models\Tickets;
use yii\helpers\Html;
use yii\db\Query;

class SiteController extends Controller {
    
    public function actionEnlace($texto, $enlace) {
        return Html::a($texto, [$enlace]);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['administracion', 'reparaciones', 'carrito', 'crear-carrito'],
                'rules' => [
                    [
                        'actions' => ['administracion'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->isAdmin(Yii::$app->user->identity->cd_ticket);
                        },
                    ],
                    [
                        'actions' => ['reparaciones', 'carrito', 'crear-carrito'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest;
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->session->setFlash('loginSuccess', '¡Inicio de sesión exitoso!');
            return $this->goHome();
        } else if ($model->hasErrors()) {
            Yii::$app->session->setFlash('loginError', '¡Algo salio mal! Informanos del error o prueba otra vez.');
        }

        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();
        Yii::$app->session->setFlash('logout', 'Se ha cerrado tu sesión con éxito. ¡Hasta luego!');
        
        return $this->goHome();
    }
    
    public function actionSingin() {
        $model = new Tickets();
        $model->cd_ticket = 'T' . rand(10000, 99999);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->actionLogin();
        }

        return $this->render('singin', [
            'model' => $model,
        ]);
    }

    /**
     * Displays policies page.
     *
     * @return Response|string
     */
    public function actionPoliticas() {
        return $this->render('politicas');
    }

    public function actionAdministracion() {
        $nombres = [
            'Tabla Productos',
            'Tabla Empleados',
            'Tabla Tickets',
            'Tabla Ventas',
            'Tabla Proveedores',
            'Tabla Reparaciones',
            'Tabla Telefonos',
            'Tabla Relación Venta-Producto'
        ];

        $enlaces = [
            'productos/',
            'empleados/',
            'tickets/',
            'ventas/',
            'proveedores/',
            'reparaciones/',
            'telefonos/',
            'rventasproductos/',
        ];
        
        $dataProvider = new ActiveDataProvider([
            'query' => Reparaciones::find()->where(['estado' => null, 'cd_empleado' => null]),
        ]);

        return $this->render('administracion', [
                    'nombres' => $nombres,
                    'enlaces' => $enlaces,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDetalleCatalogo($id) {
        $producto = Productos::findOne($id);
        $proveedor = Proveedores::findOne($producto->cd_proveedor);
        $telefonos = Telefonos::find()->where(['cd_proveedor' => $proveedor->cd_proveedor])->all();
        if (Yii::$app->session->hasFlash('cantidad')) {
            $cantidad = Yii::$app->session->getFlash('cantidad');
        } else {
            $cantidad = null;
        }
        return $this->render('detallecatalogo', ['producto' => $producto,
                    'proveedor' => $proveedor,
                    'telefonos' => $telefonos,
                    'cantidad' => $cantidad,
        ]);
    }

    public function actionCatalogo() {
        $dataProvider = new ActiveDataProvider([
            'query' => Productos::find(),
        ]);
        return $this->render('catalogo', [
                    "dataProvider" => $dataProvider,
        ]);
    }
    
    public function actionAceptarPoliticas() {
        Yii::$app->session->setFlash('politicas', $this->renderPartial('politicas'));
        $cantidad = Yii::$app->request->post('cantidad');
        Yii::$app->session->setFlash('cantidad', $cantidad);
        $id = Yii::$app->request->post('cd_producto');
        return  $this->actionDetalleCatalogo($id);
    }
    
    public function actionReparaciones() {
        $dataProvider = new ActiveDataProvider([
            'query' => Reparaciones::find()
                    ->innerJoinWith('nVenta')
                    ->where(['ventas.cd_ticket' => Yii::$app->user->id])
                    ->andWhere(['IS NOT', 'cd_empleado', null])
                    ->with('cdEmpleado'),
        ]);
        return $this->render('reparaciones', [
                    "dataProvider" => $dataProvider,
        ]);
    }

    public function actionAprobarReparaciones($id) {
        $reparacion = Reparaciones::findOne(['cd_reparacion' => $id]);
        $reparacion->estado = 0;
        $reparacion->save();

        Yii::$app->session->setFlash('aprobarReparacion', 'Su reparación está en la cola y será atendida en breve.');
        return $this->actionReparaciones();
    }
    
    public function actionCancelarReparaciones($id) {
        $reparacion = Reparaciones::findOne($id);
        $reparacion->delete();
        Yii::$app->session->setFlash('cancelarReparacion', 'Lamentamos no continuar con la reparación.');
        return $this->actionReparaciones();
    }
    
    public function actionFormReparaciones($id) {
        $model = null;
        if ($id !== 'newForm') {
            $model = Reparaciones::findOne($id);
        } else {
            $model = new Reparaciones();
            $venta = Ventas::findOne(['cd_ticket' => Yii::$app->user->id]);
            $model->n_venta = $venta->n_venta;
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('crearReparacion', 'Reparación creada con éxito.');
            return $this->actionReparaciones();
        }

        return $this->render('formreparaciones', ['model' => $model]);
    }

    public function actionCarrito() {
        if (!Yii::$app->session->has('carrito')) {
            Yii::$app->session->set('carrito', []);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => Rventasproductos::find()
                    ->with('cdProducto')
                    ->where([
                        'n_venta' =>
                        (new Query())
                        ->select('n_venta')
                        ->from('ventas')
                        ->where(['cd_ticket' => Yii::$app->user->id])
                    ]),
        ]);
        $carrito = Yii::$app->session->get('carrito', []);
        return $this->render('carrito', [
                    "dataProvider" => $dataProvider,
                    "carrito" => $carrito,
        ]);
    }

    public function actionCrearCarrito() {
        $id = Yii::$app->request->post('id');
        $cantidad = Yii::$app->request->post('cantidad');
        $producto = Productos::findOne($id);
        if (!Yii::$app->session->has('carrito')) {
            Yii::$app->session->set('carrito', []);
        }
        $carrito = Yii::$app->session->get('carrito', []);

        $sumarDuplicados = null;
        foreach ($carrito as $indice => $productoCarrito) {
            if ($productoCarrito['id'] == $id) {
                $sumarDuplicados = $indice;
                break;
            }
        }

        if ($sumarDuplicados !== null) {
            $cantidad = intval($cantidad);
            $carrito[$sumarDuplicados]['cantidad'] += $cantidad;
        } else {
            $cantidad = intval($cantidad);
            $carrito[] = [
                'id' => $producto->cd_producto,
                'cantidad' => $cantidad,
                'nombre' => $producto->nombre,
                'precio' => $producto->precio,
            ];
        }
        Yii::$app->session->set('carrito', $carrito);

        Yii::$app->session->setFlash('crearCarrito', 'El producto se ha agregado al carrito.');
        return $this->actionCarrito();
    }

    public function actionGuardarCarrito() {
        $carrito = Yii::$app->session->get('carrito');
        $venta = Ventas::findOne(['cd_ticket' => Yii::$app->user->id]);

        foreach ($carrito as $item) {
            $cdProducto = $item['id'];
            $cantidad = $item['cantidad'];

            $sumarDuplicados = Rventasproductos::findOne(['n_venta' => $venta->n_venta, 'cd_producto' => $cdProducto]);

            if ($sumarDuplicados !== null) {
                $sumarDuplicados->cantidad += $cantidad;
                $sumarDuplicados->save();
            } else {
                $registro = new Rventasproductos();
                $registro->n_venta = $venta->n_venta;
                $registro->cd_producto = $cdProducto;
                $registro->cantidad = $cantidad;
                $registro->save();
            }
        }
        Yii::$app->session->remove('carrito');

        Yii::$app->session->setFlash('guardarCarrito', 'Los productos se han agregado a Pendientes de Envio');
        return $this->actionCarrito();
    }
    
    public function actionEliminarCarrito($id) {
        $carrito = Yii::$app->session->get('carrito', []);
        foreach ($carrito as $key => $item) {
            if ($item['id'] == $id) {
                unset($carrito[$key]);
                break;
            }
        }

        Yii::$app->session->set('carrito', $carrito);

        Yii::$app->session->setFlash('eliminarCarrito', 'Se ha eliminado el producto del carrito');
        return $this->actionCarrito();
    }

    public function actionModificarCarrito($id) {
        $carrito = Yii::$app->session->get('carrito', []);
        foreach ($carrito as $key => $item) {
            if ($item['id'] == $id) {
                unset($carrito[$key]);
                break;
            }
        }

        Yii::$app->session->set('carrito', $carrito);
        return $this->actionDetalleCatalogo($id);
    }
    
    public function actionCancelarCarrito() {
        Yii::$app->session->setFlash('cancelarCarrito', 'Revisa nuestras politicas de devoluciones y cancelaciones y ponte en contacto con nosotros.
            <br>Email: <a href="serviciocliente@tacorp.com">serviciocliente@tacorp.com</a>
            <br>Teléfono: 888 77 66 55');
        return $this->actionCarrito();
    }
}
