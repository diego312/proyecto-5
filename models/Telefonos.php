<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos_proveedores".
 *
 * @property string $telefono
 * @property string $cd_proveedor
 *
 * @property Proveedores $cdProveedor
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos_proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono', 'cd_proveedor'], 'required'],
            [['telefono'], 'string', 'max' => 18],
            [['cd_proveedor'], 'string', 'max' => 9],
            [['telefono'], 'unique'],
            [['cd_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::class, 'targetAttribute' => ['cd_proveedor' => 'cd_proveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'telefono' => 'Telefono',
            'cd_proveedor' => 'Codigo Proveedor',
        ];
    }

    /**
     * Gets query for [[CdProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCdProveedor()
    {
        return $this->hasOne(Proveedores::class, ['cd_proveedor' => 'cd_proveedor']);
    }
}
