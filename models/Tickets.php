<?php

namespace app\models;

use Yii;
use yii\base\Security;

/**
 * This is the model class for table "tickets".
 *
 * @property string $cd_ticket
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $direccion
 * @property string|null $email
 * @property string|null $telefono
 * @property string|null $password
 *
 * @property Ventas[] $ventas
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cd_ticket'], 'required'],
            [['cd_ticket'], 'string', 'max' => 6],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['apellidos', 'direccion', 'email'], 'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 15],
            ['password', 'string', 'max' => 64],
            [['cd_ticket'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cd_ticket' => 'Cd Ticket',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'direccion' => 'Direccion',
            'email' => 'Email',
            'telefono' => 'Telefono',
            'password' => 'Contraseña'
        ];
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['cd_ticket' => 'cd_ticket']);
    }
}
