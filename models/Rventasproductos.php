<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "r_ventas_productos".
 *
 * @property int $id
 * @property int $n_venta
 * @property int $cd_producto
 * @property int $cantidad
 *
 * @property Productos $cdProducto
 * @property Ventas $nVenta
 */
class Rventasproductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'r_ventas_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_venta', 'cd_producto', 'cantidad'], 'required'],
            [['n_venta', 'cd_producto', 'cantidad'], 'integer'],
            [['n_venta'], 'exist', 'skipOnError' => true, 'targetClass' => Ventas::class, 'targetAttribute' => ['n_venta' => 'n_venta']],
            [['cd_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['cd_producto' => 'cd_producto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'n_venta' => 'N Venta',
            'cd_producto' => 'Cd Producto',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[CdProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCdProducto()
    {
        return $this->hasOne(Productos::class, ['cd_producto' => 'cd_producto']);
    }

    /**
     * Gets query for [[NVenta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNVenta()
    {
        return $this->hasOne(Ventas::class, ['n_venta' => 'n_venta']);
    }
}
