<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property string $dni
 * @property string $nombre
 * @property string $apellidos
 * @property string $direccion
 * @property string $email
 * @property string $telefono
 *
 * @property Reparaciones[] $reparaciones
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'nombre', 'apellidos', 'direccion', 'email', 'telefono'], 'required'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['apellidos', 'direccion', 'email'], 'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 15],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'direccion' => 'Direccion',
            'email' => 'Email',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[Reparaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReparaciones()
    {
        return $this->hasMany(Reparaciones::class, ['dni_empleado' => 'dni']);
    }
}
