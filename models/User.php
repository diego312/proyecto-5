<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {

    private static $users = [
        'admin' => [
            'cd_ticket' => 'admin@tacorp.com',
            'nombre' => 'Administrador',
            'password' => '@DM1N',
        ],
    ];
    
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'tickets';
    }

    /**
     * {@inheritdoc}
     */
    public static function primaryKey() {
        return ['cd_ticket'];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        foreach (self::$users as $user) {
            if (strcasecmp($user['cd_ticket'], $id) === 0) {
                return new static($user);
            }
        }
        return static::findOne(['cd_ticket' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        foreach (self::$users as $user) {
            if (strcasecmp($user['cd_ticket'], $username) === 0) {
                return new static($user);
            }
        }
        return static::findOne(['cd_ticket' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        if ($this->cd_ticket !== null) {
            return $this->cd_ticket;
        }
        
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        return null;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === $password;
    }

    public function isAdmin($value)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['cd_ticket'], $value) === 0) {
                return true;
            }
        }
        return false;
    }
}