<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reparaciones;

/**
 * ReparacionesSearch represents the model behind the search form of `app\models\Reparaciones`.
 */
class ReparacionesSearch extends Reparaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cd_reparacion', 'coste', 'n_venta'], 'integer'],
            [['motivo', 'mot_detalles', 'diagnostico', 'f_recepcion', 'cd_empleado'], 'safe'],
            [['estado'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reparaciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cd_reparacion' => $this->cd_reparacion,
            'f_recepcion' => $this->f_recepcion,
            'coste' => $this->coste,
            'estado' => $this->estado,
            'n_venta' => $this->n_venta,
        ]);

        $query->andFilterWhere(['like', 'motivo', $this->motivo])
            ->andFilterWhere(['like', 'mot_detalles', $this->mot_detalles])
            ->andFilterWhere(['like', 'diagnostico', $this->diagnostico])
            ->andFilterWhere(['like', 'cd_empleado', $this->cd_empleado]);

        return $dataProvider;
    }
}
