<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $n_venta
 * @property string $f_entrega
 * @property string|null $cd_ticket
 *
 * @property Tickets $cdTicket
 * @property RVentasProductos[] $rVentasProductos
 * @property Reparaciones[] $reparaciones
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_entrega'], 'required'],
            [['f_entrega'], 'safe'],
            [['cd_ticket'], 'string', 'max' => 6],
            [['cd_ticket'], 'exist', 'skipOnError' => true, 'targetClass' => Tickets::class, 'targetAttribute' => ['cd_ticket' => 'cd_ticket']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'n_venta' => 'N Venta',
            'f_entrega' => 'F Entrega',
            'cd_ticket' => 'Cd Ticket',
        ];
    }

    /**
     * Gets query for [[CdTicket]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCdTicket()
    {
        return $this->hasOne(Tickets::class, ['cd_ticket' => 'cd_ticket']);
    }

    /**
     * Gets query for [[RVentasProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRVentasProductos()
    {
        return $this->hasMany(RVentasProductos::class, ['n_venta' => 'n_venta']);
    }

    /**
     * Gets query for [[Reparaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReparaciones()
    {
        return $this->hasMany(Reparaciones::class, ['n_venta' => 'n_venta']);
    }
}
