<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $cd_producto
 * @property string $nombre
 * @property int $precio
 * @property string $descripcion
 * @property string $nif_proveedor
 *
 * @property Proveedores $nifProveedor
 * @property Rventasproductos[] $rventasproductos
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'precio', 'descripcion', 'cd_proveedor'], 'required'],
            [['precio'], 'integer'],
            [['nombre'], 'string', 'max' => 40],
            [['descripcion'], 'string', 'max' => 200],
            [['cd_proveedor'], 'string', 'max' => 9],
            [['cd_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::class, 'targetAttribute' => ['cd_proveedor' => 'cd_proveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cd_producto' => 'Cd Producto',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'descripcion' => 'Descripcion',
            'cd_proveedor' => 'Codigo Proveedor',
        ];
    }

    /**
     * Gets query for [[cd_proveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCdProveedor()
    {
        return $this->hasOne(Proveedores::class, ['cd_proveedor' => 'cd_proveedor']);
    }

    /**
     * Gets query for [[Rventasproductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRventasproductos()
    {
        return $this->hasMany(Rventasproductos::class, ['cd_producto' => 'cd_producto']);
    }
}
