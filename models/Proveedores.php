<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property string $nif
 * @property string $nombre
 * @property string $direccion
 * @property string $email
 *
 * @property Productos[] $productos
 * @property TelefonosProveedores[] $telefonosProveedores
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nif', 'nombre', 'direccion', 'email'], 'required'],
            [['nif'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 20],
            [['direccion', 'email'], 'string', 'max' => 50],
            [['nif'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cd_proveedor' => 'Id',
            'nif' => 'Nif',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'email' => 'Email',
        ];
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Productos::class, ['cd_proveedor' => 'cd_proveedor']);
    }

    /**
     * Gets query for [[TelefonosProveedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonosProveedores()
    {
        return $this->hasMany(TelefonosProveedores::class, ['cd_proveedor' => 'cd_proveedor']);
    }
}
