<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reparaciones".
 *
 * @property int $cd_reparacion
 * @property string $motivo
 * @property string $mot_detalles
 * @property string $diagnostico
 * @property string $f_recepcion
 * @property int $coste
 * @property bool $estado
 * @property int $n_venta
 * @property string $cd_empleado
 *
 * @property Empleados $cdEmpleado
 * @property Ventas $nVenta
 */
class Reparaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reparaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['motivo', 'mot_detalles', 'n_venta'], 'required'],
            [['f_recepcion'], 'safe'],
            [['coste', 'n_venta', 'cd_empleado'], 'integer'],
            [['estado'], 'boolean'],
            [['motivo'], 'string', 'max' => 40],
            [['mot_detalles', 'diagnostico'], 'string', 'max' => 200],
            [['cd_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::class, 'targetAttribute' => ['cd_empleado' => 'cd_empleado']],
            [['n_venta'], 'exist', 'skipOnError' => true, 'targetClass' => Ventas::class, 'targetAttribute' => ['n_venta' => 'n_venta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cd_reparacion' => 'Id',
            'motivo' => 'Motivo',
            'mot_detalles' => 'Mot Detalles',
            'diagnostico' => 'Diagnostico',
            'f_recepcion' => 'F Recepcion',
            'coste' => 'Coste',
            'estado' => 'Estado',
            'n_venta' => 'N Venta',
            'cd_empleado' => 'Cd Empleado',
        ];
    }

    /**
     * Gets query for [[CdEmpleado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCdEmpleado()
    {
        return $this->hasOne(Empleados::class, ['cd_empleado' => 'cd_empleado']);
    }

    /**
     * Gets query for [[NVenta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNVenta()
    {
        return $this->hasOne(Ventas::class, ['n_venta' => 'n_venta']);
    }
}
